package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			if (text.length() > width)
				throw new IndexOutOfBoundsException("The string given is longer then the width");
			
			int extra = (width - text.length()) / 2;
			int textLength = (extra*2) + text.length();
			
			if (textLength == width)
				return " ".repeat(extra) + text + " ".repeat(extra);
			else
				return " ".repeat(extra) + text + " ".repeat(extra + 1);
		}

		public String flushRight(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}

		public String flushLeft(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}

		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}};
		
	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals(" fo  ", aligner.center("fo", 5));
		assertEquals("  fo   ", aligner.center("fo", 7));
		assertEquals("  foo   ", aligner.center("foo", 8));
	}
	
	@Test
    void testIndexOutrOfBoundsException() {
        Boolean thrown = false;

        try {
            aligner.center("null", 0);
        }
        catch (IndexOutOfBoundsException e) {
            thrown = true;
        }
        assertTrue(thrown);
        
    }
	

}
